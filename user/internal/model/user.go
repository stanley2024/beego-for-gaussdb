package model

import (
	"time"

	"github.com/beego/beego/v2/client/orm"
)

type User struct {
	ID           uint       `orm:"pk;auto;column(id)"` // Standard field for the primary key
	Name         string     `orm:"size(100)"`          // 一个常规字符串字段
	Email        *string    // 一个指向字符串的指针, allowing for null values
	Age          uint8      // 一个未签名的8位整数
	Birthday     *time.Time // A pointer to time.Time, can be null
	MemberNumber *string    // Uses sql.NullString to handle nullable strings
	ActivatedAt  *time.Time // Uses sql.NullTime for nullable time fields
	CreatedAt    time.Time  // 创建时间（由GORM自动管理）
	UpdatedAt    time.Time  // 最后一次更新时间（由GORM自动管理）
}

func init() {
	orm.RegisterModel(new(User))
}

func (u *User) TableName() string {
	return "users"
}
