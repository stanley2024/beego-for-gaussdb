package svc

import (
	"gitcode.com/HuaweiCloudDeveloper/OpenSourceForHuaweiDemoGo/user/internal/config"
	_ "gitee.com/opengauss/openGauss-connector-go-pq"

	"github.com/beego/beego/v2/client/orm"
)

type ServiceContext struct {
	Config config.Config
	DB     orm.Ormer
}

func NewServiceContext(c config.Config) *ServiceContext {
	// 注册 postgres 驱动，使用 GaussDB 驱动库
	orm.RegisterDriver("postgres", orm.DRPostgres)
	orm.RegisterDataBase("default", "postgres", c.DSN)

	return &ServiceContext{
		Config: c,
		DB:     orm.NewOrm(),
	}
}
