package logic

import (
	"context"
	"fmt"
	"time"

	"gitcode.com/HuaweiCloudDeveloper/OpenSourceForHuaweiDemoGo/user/internal/model"
	"gitcode.com/HuaweiCloudDeveloper/OpenSourceForHuaweiDemoGo/user/internal/svc"
	"gitcode.com/HuaweiCloudDeveloper/OpenSourceForHuaweiDemoGo/user/internal/types"

	"github.com/zeromicro/go-zero/core/logx"
)

type UpdateLogic struct {
	logx.Logger
	ctx    context.Context
	svcCtx *svc.ServiceContext
}

func NewUpdateLogic(ctx context.Context, svcCtx *svc.ServiceContext) *UpdateLogic {
	return &UpdateLogic{
		Logger: logx.WithContext(ctx),
		ctx:    ctx,
		svcCtx: svcCtx,
	}
}

func (l *UpdateLogic) Update(req *types.UpdateUser) (resp *types.Response, err error) {
	// todo: add your logic here and delete this line
	user := model.User{ID: uint(req.Id)}
	err = l.svcCtx.DB.Read(&user)
	if err != nil {
		return nil, fmt.Errorf("user not found")
	}

	if req.Name != "" {
		user.Name = req.Name
	}
	if req.Email != "" {
		user.Email = &req.Email
	}
	if req.Age != 0 {
		user.Age = uint8(req.Age)
	}
	if req.Birthday != "" {
		t, _ := time.Parse(req.Birthday, "2006-01-02")
		user.Birthday = &t
	}

	num, err := l.svcCtx.DB.Update(&user)
	if err != nil {
		return nil, err
	}

	resp = new(types.Response)
	resp.Message = fmt.Sprintf("success, updated %d", num)
	return
}
